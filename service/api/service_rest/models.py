from django.db import models
from django.urls import reverse


class Status(models.Model):
    id = models.PositiveSmallIntegerField(primary_key=True)
    name = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return self.name


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, null=True)
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=True)


class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=20, unique=True)


    def get_api_url(self):
        return reverse("api_technician_list", kwargs={"pk": self.employee_id})


class Appointment(models.Model):
    def get_submitted():
        return Status.objects.get(name="sub")


    date_time = models.DateTimeField()
    reason = models.TextField()
    status = models.ForeignKey(
        Status,
        default=get_submitted,
        related_name="appointments",
        on_delete= models.PROTECT,
    )


    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )


    def finished(self):
        status = Status.objects.get(name="Finished")
        self.status = status
        self.save()


    def cancelled(self):
        status = Status.objects.get(name="Cancelled")
        self.status = status
        self.save()


    def get_api_url(self):
        return reverse("api_appointment_list", kwargs={"pk": self.id})
