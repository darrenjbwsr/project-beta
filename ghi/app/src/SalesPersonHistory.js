import React, {useEffect, useState,} from 'react';

function SalesPersonHistory(){
    const [salespeople, setSalesPeople]= useState([]);
    const [selectSalesPerson, setSelectSalesPerson]= useState('');
    const [sales, setSales]= useState([]);

    const fetchSalespeople = async () =>{
        const salesPeopleUrl = 'http://localhost:8090/api/salespeople/';

        const response = await fetch(salesPeopleUrl);

        if (response.ok) {
        const data2 = await response.json();
        setSalesPeople(data2.salespeople);
        }
    }

    async function fetchSales() {
        const salesUrl = 'http://localhost:8090/api/sales/';
        const response = await fetch(salesUrl);

        if (response.ok) {
        const data = await response.json();
        setSales(data.sales);
        }
    }
    useEffect(() => {fetchSalespeople(); fetchSales();}, []);

    const handleSalesPersonChange = (event)=> {
        const value = event.target.value;
        setSelectSalesPerson(value);
    }

    return(
        <div>
            <div>
            <h1>Salesperson History</h1>
            </div>
            <form>
            <div className="mb-3">
            <select onChange={handleSalesPersonChange} required id="salesperson" name="salesperson" value={selectSalesPerson}  className="form-select">
                <option value="">Salesperson</option>
                    {salespeople.map(people => {
                        return (
                            <option key={people.id} value={people.employee_id} >
                                {people.first_name}
                            </option>
                        );
                    })}
            </select>
            </div>
            </form>
        <table className = "table table-striped">
            <thead>
                <tr>
                <th>Salesperson</th>
                <th>Customer</th>
                <th>Vin</th>
                <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {sales.filter(sale => sale.salesperson.employee_id === selectSalesPerson).map( sale => {
                    return(
                        <tr key={sale.id} >
                            <td>{sale.salesperson.first_name}</td>
                            <td>{sale.customer.first_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                    );
                })}

            </tbody>
        </table>
        </div>
    )
}

export default SalesPersonHistory;
