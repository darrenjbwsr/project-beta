import React, { useEffect, useState,} from 'react';

function SalesList(props){
    const [sales, setSales] = useState([]);

    async function loadSales() {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
        }
        }
        useEffect(() => {loadSales();}, []);

    return(
        <div>
        <div>
            <h1>Sales</h1>
        </div>
        <table className = "table table-striped">
        <thead>
            <tr>
            <th>Salesperson Employee ID</th>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>Vin</th>
            <th>Price</th>
            </tr>
        </thead>
        <tbody>
            {sales.map(sales => {
                return(
                    <tr key={sales.id} value={sales.href} > 
                        <td>{sales.salesperson.employee_id}</td>
                        <td>{sales.salesperson.first_name}</td>
                        <td>{sales.customer.first_name}</td>
                        <td>{sales.automobile.vin}</td>
                        <td>{sales.price}</td>
                    </tr>
                );
            })}
        </tbody>
        </table>
        </div>
    )
}

export default SalesList;
