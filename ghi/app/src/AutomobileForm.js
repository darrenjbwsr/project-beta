import React, {useEffect, useState,} from 'react';

function AutomobileForm(){
    const [models, setModels] = useState([]);
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [model, setModel] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            setModels(data.models);
        }
    }
    useEffect(() => {fetchData();}, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data={};
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model;

    const autosUrl = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(autosUrl, fetchConfig);
    if (response.ok){
        setColor('');
        setYear('');
        setVin('');
        setModel('');
    }
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handleYearChange = (event) => {
        const value = event.target.value;
        setYear(value);
    }
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }
    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value);
    }
    return(
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Add an Automobile to Inventory</h1>
            <form onSubmit={handleSubmit} id="create-vehicle-form">
            <div className="form-floating mb-3">
                <input onChange={handleColorChange} placeholder="color" required type="text" id="color" name="color" value={color} className="form-control"/>
                <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleYearChange} placeholder="year" type="number" id="year" name="year" value={year} className="form-control"/>
                <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleVinChange} placeholder="vin" type="number" id="vin" name="vin" value={vin} className="form-control"/>
                <label htmlFor="vin">VIN</label>
            </div>
            <div className="mb-3">
            <select onChange={handleModelChange} required id="model" name="model" value={model}  className="form-select">
                <option value=" ">Choose a Model</option>
                    {models.map(model => {
                        return (
                            <option key={model.id} value={model.id}>
                                {model.name}
                            </option>
                        )
                    })}
            </select>
            </div>
            <button className="btn btn-primary me-md-4">Create</button>
            </form>
            </div>
            </div>
        </div>
    )
}

export default AutomobileForm;
