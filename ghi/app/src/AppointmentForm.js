import React, { useState, useEffect } from 'react'
function AppointmentForm() {
    const [technicians, setTechnicians] = useState([])
    const [date, setDate] = useState('')
    const [time, setTime] = useState('')
    const [formData, setFormData] = useState({
        vin:'',
        customer:'',
        technician:'',
        reason:'',
    })


    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setTechnicians(data.technicians)
        }
    }


    useEffect(() => {
        fetchData()
    }, [])



    const handleFormChange = (event) => {
        const value = event.target.value
        const name = event.target.name
        setFormData({...formData, [name]: value})
    }


    const handleDateChange = (event) => {
        const value = event.target.value
        setDate(value)
    }


    const handleTimeChange = (event) => {
        const value = event.target.value
        setTime(value)
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const date_time = new Date(`${date} ${time}`)
        const updatedFormData = {...formData}
        updatedFormData.date_time = date_time
        const url = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(updatedFormData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setFormData({
                vin:'',
                customer:'',
                technician:'',
                reason:'',
            });
            setDate('')
            setTime('')
        }
    }


    return (
        <div className="row">
        <div className="offset-3 col-7">
        <div className="shadow p-4 mt-4">
            <h1>Request an Appointment</h1>
            <form onSubmit={handleSubmit} id="create-technician-form">
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} value={formData.vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control"/>
                    <label htmlFor="vin">Automobile Vin</label>
                </div>
                <div className="form-floating mb-3">
                <   input onChange={handleFormChange} value={formData.customer} placeholder="Customer Name" required type="text" name="customer" id="customer" className="form-control"/>
                    <label htmlFor="last_name">Customer Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleDateChange} value={date} placeholder="mm/dd/yyyy" required type="date" name="date" id="date" className="form-control"/>
                    <label htmlFor="date">Date</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleTimeChange} value={time} placeholder="--:-- --" required type="time" name="time" id="time" className="form-control"/>
                    <label htmlFor="date">Time</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleFormChange} value = {formData.technician} id="technician" name = "technician" required className="form-select">
                    <option value="">Choose a Technician</option>
                    {technicians.map(technician => {
                  return (
                    <option key={technician.id} value={technician.id}>{technician.first_name} {technician.last_name}</option>
                  )
                })}
                </select>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control"/>
                <label htmlFor="reason">Reason</label>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
            </div>
    );
}

export default AppointmentForm
