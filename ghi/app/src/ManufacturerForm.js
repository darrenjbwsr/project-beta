import React, {useState} from 'react';

function ManufacturerForm(){
    const [manufacturerName, setManufacturerName] = useState('')

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data={};
        data.name = manufacturerName;

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers:{
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(manufacturerUrl, fetchConfig);
        if(response.ok){
            setManufacturerName('');
        }
        }

    const handleManufacturerNameChange = (event) => {
        const value = event.target.value;
        setManufacturerName(value);
        }

    return(
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Add a Manufacturer</h1>
            <form onSubmit={handleSubmit} id="create-manufacturer-form">
            <div className="form-floating mb-3">
                <input onChange={handleManufacturerNameChange} placeholder="manufacturer_name" required type="text" id="manufacturer_name" name="manufacturer_name" value={manufacturerName} className="form-control"/>
                <label htmlFor="manufacturer_name">Manufacturer Name</label>
            </div>
            <button className="btn btn-primary me-md-4">Create</button>
            </form>
            </div>
            </div>
        </div>
    )
}

export default ManufacturerForm;
