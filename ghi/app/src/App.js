import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelList from './VehicleModelList';
import VehicleModelForm from './VehicleModelForm';
import AutomobilesList from './AutomobilesList';
import AutomobileForm from './AutomobileForm';
import SalesPersonForm from './SalesPersonForm';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import SalesPeopleList from './SalesPeopleList';
import SalesPersonHistory from './SalesPersonHistory';
import SalesList from './SalesList';
import NewSale from './NewSale';
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import AppointmentHistory from './AppointmentHistory';
import AppointmentList from './AppointmentList';


function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route path= "list" element= {<ManufacturersList manufacturers={props.manufacturers} />} />
            <Route path= "new" element= {<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route path= "list" element= {<VehicleModelList  />} />
            <Route path= "new" element= {<VehicleModelForm />} />
          </Route>
          <Route path="automobiles">
            <Route path= "list" element= {<AutomobilesList  />} />
            <Route path= "new" element= {<AutomobileForm />} />
          </Route>
          <Route path="salesperson">
            <Route path= "list" element= {<SalesPeopleList salespeople={props.salespeople} />} />
            <Route path= "new" element= {<SalesPersonForm />} />
            <Route path= "history" element= {<SalesPersonHistory  />} />
          </Route>
          <Route path="customers">
            <Route path= "list" element= {<CustomerList customers={props.customers} />} />
            <Route path= "new" element= {<CustomerForm />} />
          </Route>
          <Route path="sales">
            <Route path= "list" element= {<SalesList sales={props.sales}/>} />
            <Route path= "new" element= {<NewSale />} />
          </Route>
          <Route path="technicians">
            <Route path="list" element={<TechnicianList/>}/>
            <Route path="new" element={<TechnicianForm/>}/>
          </Route>
          <Route path="appointments">
            <Route path="list" element={<AppointmentList/>}/>
            <Route path="new" element={<AppointmentForm/>}/>
            <Route path="history" element={<AppointmentHistory/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
