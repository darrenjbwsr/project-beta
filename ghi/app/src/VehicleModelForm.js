import React, {useEffect, useState,} from 'react';

function VehicleModelForm(){
    const [manufacturers, setManufacturers] = useState([]);
    const [modelName, setModelName] = useState('');
    const [picture, setPicture] = useState('');
    const [manufacturerName, setManufacturerName] = useState('');

    const fetchData = async () =>{
        const url = 'http://localhost:8100/api/manufacturers/';

        const response = await fetch(url);

        if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
        }
    }
    useEffect(() => {fetchData();}, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data={};
        data.name = modelName;
        data.picture_url = picture;
        data.manufacturer_id = manufacturerName;

    const modelsUrl = 'http://localhost:8100/api/models/';
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(modelsUrl, fetchConfig);
    if (response.ok) {
        const newVehicle= await response.json();
        setModelName('');
        setPicture('');
        setManufacturerName('');
    } 
    }
    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }
    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturerName(value);
    }
    return(
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Add a Vehicle</h1>
            <form onSubmit={handleSubmit} id="create-vehicle-form">
            <div className="form-floating mb-3">
                <input onChange={handleModelNameChange} placeholder="model_name" required type="text" id="model_name" name="model_name" value={modelName} className="form-control"/>
                <label htmlFor="model_name">Model Name</label>
            </div>    
            <div className="form-floating mb-3">
                <input onChange={handlePictureChange} placeholder="picture_url" type="url" id="picture" name="picture" value={picture} className="form-control"/>
                <label htmlFor="picture">Picture Url</label>
            </div>
            <div className="mb-3">
            <select onChange={handleManufacturerChange} required id="manufacturer" name="manufacturer" value={manufacturerName}  className="form-select">
                <option value=" ">Choose a Manufacturer</option>
                    {manufacturers.map(manufacturer => {
                        return (
                            <option key={manufacturer.href} value={manufacturer.id}>
                                {manufacturer.name}
                            </option>
                        );
                    })}
            </select>
            </div>
            <button className="btn btn-primary me-md-4">Create</button>
            </form>
            </div>
            </div>
        </div>
    )
}

export default VehicleModelForm;
