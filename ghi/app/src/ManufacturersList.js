import React, { useEffect, useState,} from 'react';

function ManufacturersList(){
    const [manufacturers, setManufacturers] = useState([]);

    async function loadManufacturers() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok){
            const data = await response.json();
            setManufacturers(data.manufacturers)
        }
    }
    useEffect(() => {loadManufacturers();}, []);

    return(
        <div>
            <div>
                <h1>Manufacturers</h1>
            </div>
            <table className = "table table-striped">
            <thead>
                <tr>
                <th>Name</th>
                </tr>
            </thead>
            <tbody>
                {manufacturers.map(manufacturer => {
                    return(
                        <tr key={manufacturer.id} value={manufacturer.id} > 
                            <td>{manufacturer.name}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    </div>
    )
}

export default ManufacturersList;
